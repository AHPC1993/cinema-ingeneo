#base de datos para software de control de cinemas.

create schema bd_cinema;
grant all privileges on *.* to 'admincinema'@'localhost' identified by 'cinema123';
drop table bd_cinema.pago;
drop table bd_cinema.reserva;
drop table bd_cinema.agenda;
drop table bd_cinema.pelicula_sucursal;
drop table bd_cinema.pelicula;
drop table bd_cinema.sala_filas;
drop table bd_cinema.cliente;
drop table bd_cinema.sala;
drop table bd_cinema.pa_fila;
drop table bd_cinema.pa_tipo_sala;
drop table bd_cinema.sucursal;
drop table bd_cinema.administrador;
drop table bd_cinema.ciudad;
drop table bd_cinema.pa_departamento;



#creación de la tabla paramétrica de los departamentos
create table if not exists bd_cinema.pa_departamento(
id_departamento int,
nombre varchar(50) not null,
observaciones varchar(255),
primary key(id_departamento)
)engine=innodb;

#creación de la tabla encargada de almacenar las ciudades
create table if not exists bd_cinema.ciudad(
id_ciudad int auto_increment,
nombre varchar(50) not null,
id_departamento int not null,
observaciones varchar(255),
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_ciudad),
foreign key (id_departamento)
references bd_cinema.pa_departamento(id_departamento)
)engine=innodb;

#creación de la tabla encargada de almacenar los administradores de las salas
create table if not exists bd_cinema.administrador(
id_administrador int auto_increment,
nombre varchar(50) not null,
cedula varchar(10) not null,
telefono varchar(13) not null,
direccion varchar(100) not null,
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_administrador)
)engine=innodb;


#creación de la tabla encargada de almacenar los clientes registrados
create table if not exists bd_cinema.cliente(
id_cliente int auto_increment,
nombre varchar(50) not null,
cedula varchar(10) not null,
telefono varchar(13) not null,
direccion varchar(100) not null,
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_cliente)
)engine=innodb;

#creación de la tabla encargada de almacenar las sucursales de una ciudad
create table if not exists bd_cinema.sucursal(
id_sucursal int auto_increment,
nombre varchar(50) not null,
direccion varchar(100) not null,
id_administrador int not null,
id_ciudad int not null,
observaciones varchar(255),
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_sucursal),
foreign key (id_administrador)
references bd_cinema.administrador(id_administrador),
foreign key (id_ciudad)
references bd_cinema.ciudad(id_ciudad)
)engine=innodb;


#creación de la tabla paramétrica de los tipos de sala
create table if not exists bd_cinema.pa_tipo_sala(
id_tipo_sala int,
valor varchar(50) not null,
observaciones varchar(255),
primary key(id_tipo_sala)
)engine=innodb;


#creación de la tabla paramétrica de los tipos de sala, entre 3 y 10 sillas máximo
create table if not exists bd_cinema.pa_fila(
id_fila int,
nombre varchar(50) not null,
numero_sillas int not null ,
observaciones varchar(255),
primary key(id_fila),
constraint chk_numero_sillas check(numero_sillas >= 3 and numero_sillas <= 10)
)engine=innodb;

#creación de la tabla encargada de almacenar las ciudades
create table if not exists bd_cinema.sala(
id_sala int auto_increment,
nombre varchar(50) not null,
id_sucursal int not null,
id_tipo_sala int not null,
id_administrador int not null,
observaciones varchar(255),
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_sala),
foreign key (id_sucursal)
references bd_cinema.sucursal(id_sucursal),
foreign key (id_tipo_sala)
references bd_cinema.pa_tipo_sala(id_tipo_sala),
foreign key (id_administrador)
references bd_cinema.administrador(id_administrador)
)engine=innodb;

#creación de la tabla terciaria para relacionar las salas con las filas, mínimo 3 filas por sala, máximo 27
create table if not exists bd_cinema.sala_filas(
id_sala_fila int auto_increment,
id_sala int not null,
id_fila int not null,
numero_filas int not null ,
observaciones varchar(255),
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_sala_fila),
foreign key (id_sala)
references bd_cinema.sala(id_sala),
foreign key (id_fila)
references bd_cinema.pa_fila(id_fila),
constraint chk_numero_filas check(numero_filas >= 3 and numero_filas <= 27)
)engine=innodb;

#creación de la tabla encargada de almacenar las películas que estarán en una sala
create table if not exists bd_cinema.pelicula(
id_pelicula int auto_increment,
nombre_original varchar(50) not null,
nombre_traducido varchar(50) not null,
fecha_estreno datetime not null,
fecha_baja datetime not null,
genero varchar(50) not null,
sinopsis varchar(50) not null,
formato varchar(50) not null,
duracion int not null,
observaciones varchar(255),
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_pelicula)
)engine=innodb;

#creación de la tabla terciaria para almacenar la información de las películas existnetes en una sucursal
create table if not exists bd_cinema.pelicula_sucursal(
id_pelicula_sucursal int auto_increment,
id_sucursal int not null,
id_pelicula int not null,
observaciones varchar(255),
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_pelicula_sucursal),
foreign key (id_sucursal)
references bd_cinema.sucursal(id_sucursal),
foreign key (id_pelicula)
references bd_cinema.pelicula(id_pelicula)
)engine=innodb;

#creación de la tabla terciaria para agendar una película a una sala
create table if not exists bd_cinema.agenda(
id_agenda int auto_increment,
id_sala int not null,
id_pelicula int not null,
fecha datetime,
hora datetime,
observaciones varchar(255),
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_agenda),
foreign key (id_sala)
references bd_cinema.sala(id_sala),
foreign key (id_pelicula)
references bd_cinema.pelicula(id_pelicula)
)engine=innodb;

#creación de la tabla terciaria para almacenar las reservas de un cliente
create table if not exists bd_cinema.reserva(
id_reserva int auto_increment,
id_cliente int not null,
id_pelicula int not null,
fecha datetime not null,
hora datetime not null,
id_fila int not null,
numero_sillas int not null,
observaciones varchar(255),
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_reserva),
foreign key (id_cliente)
references bd_cinema.cliente(id_cliente),
foreign key (id_pelicula)
references bd_cinema.pelicula(id_pelicula)
)engine=innodb;


#creación de la tabla encargada de validar los pagos realizados por un cliente
create table if not exists bd_cinema.pago(
id_pago int auto_increment,
id_reserva int not null,
precio decimal(8,2),
pagado varchar(1),
observaciones varchar(255),
fecha_creacion datetime,
usuario_creacion varchar(30),
primary key(id_pago),
foreign key (id_reserva)
references bd_cinema.reserva(id_reserva)
)engine=innodb;


