
Encontrar un puerto en específico en uso
sudo netstat -lutnp | grep -w '<PUERTO>'

Eliminar PID que usa el puerto
kill -9 <PID>



#Creación de la tabla paramétrica para las filas de cada sala.
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(1,'A',6);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(2,'B',6);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(3,'C',7);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(4,'D',7);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(5,'E',8);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(6,'F',8);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(7,'G',9);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(8,'H',9);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(9,'I',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(10,'J',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(11,'K',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(12,'L',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(13,'M',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(14,'N',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(15,'O',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(16,'P',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(17,'Q',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(18,'R',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(19,'S',10);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(20,'T',9);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(21,'U',9);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(22,'V',9);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(23,'W',9);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(24,'X',9);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(25,'Y',9);
INSERT INTO `bd_cinema`.`pa_fila`
(`id_fila`,`nombre`,`numero_sillas`)
VALUES(26,'Z',9);


#Creación de un administrador por defecto
insert into `bd_cinema`.`administrador`
(`nombre`,
`cedula`,
`telefono`,
`direccion`)
values
(
'allan',
1053,
111,
'crr32'
);

#Paramétrica de los departamentos
insert into `bd_cinema`.`pa_departamento`
(`id_departamento`,
`nombre`,
`observaciones`
)
values
(1,'caldas','caldas un gran dpto');

#Creación de una ciudad por defecto
insert into `bd_cinema`.`ciudad`
(
`nombre`,
`id_departamento`,
`observaciones`
)
values
('manizales',1,'manizales la ciudad más bonita');

#Creación de una sucursal por defecto
insert into `bd_cinema`.`sucursal`
(`nombre`,
`direccion`,
`id_administrador`,
`id_ciudad`,
`observaciones`
)
values
('centro','cra 11', 1, 1, 'prueba');



#Creación de registros en la tabla paramétrica del tipo de sala
INSERT INTO `bd_cinema`.`pa_tipo_sala`
(`id_tipo_sala`,
`valor`,
`observaciones`)
VALUES
(1,'2D','Sala 2D');

INSERT INTO `bd_cinema`.`pa_tipo_sala`
(`id_tipo_sala`,
`valor`,
`observaciones`)
VALUES
(2,'3D','Sala 3D');

INSERT INTO `bd_cinema`.`pa_tipo_sala`
(`id_tipo_sala`,
`valor`,
`observaciones`)
VALUES
(3,'Dynamix','Sala con interacción');


commit;



