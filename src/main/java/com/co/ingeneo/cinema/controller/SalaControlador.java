package com.co.ingeneo.cinema.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.co.ingeneo.cinema.dto.SalaDTO;
import com.co.ingeneo.cinema.model.Sala;
import com.co.ingeneo.cinema.repository.SalaRepository;
import com.co.ingeneo.cinema.utils.ConstruirDTOsUtils;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,
		RequestMethod.PUT })
@RequestMapping("/controlSalaRest")
public class SalaControlador {

	/**
	 * Se crea instancia del logger para imprimir en consola
	 * 
	 */
	private static Logger logger = LoggerFactory.getLogger(SalaControlador.class.getName());

	/**
	 * Se crea instancia de la interfaz de la sala encargada de realizar todas
	 * las acciones de base de datos a través de JPA
	 * 
	 */
	@Autowired
	private SalaRepository salaRepository;

	ConstruirDTOsUtils construirDTOsUtils = new ConstruirDTOsUtils();

	/**
	 * Metodo encargado de consultar una lista de salas
	 * 
	 * @author aperez
	 * 
	 * @return SalaDTO
	 */
	@GetMapping("/consultarSalas")
	public List<SalaDTO> consultarSalas() {
		logger.info("Inicio consultarSalas");
		return construirDTOsUtils.castearListaSalasEntidadADTO(salaRepository.findAll());

	}

	/**
	 * Metodo encargado de consultar una sala por id
	 * 
	 * @author aperez
	 * 
	 * @param salaDTO
	 * @return Sala
	 */
	@GetMapping("/consultarSalaPorId/{idsala}")
	public SalaDTO consultarSalaPorId(@PathVariable(value = "idsala") Long idsala) {
		logger.info("Inicio consultarSalaPorId");
		Sala sala = salaRepository.findById(idsala).get();
		return construirDTOsUtils.castearSalaEntidadADTO(sala);
	}

	/**
	 * Metodo encargado de crear una sala
	 * 
	 * @author aperez
	 * 
	 * @param salaDTO
	 * @return Sala
	 */
	@PostMapping("/crearSala")
	public Sala crearSala(@Valid @RequestBody SalaDTO salaDTO) {
		logger.debug("Inicio crearSala");
		
		Sala SalaNueva = construirDTOsUtils.castearSalaDTOAEntidad(salaDTO);
		return salaRepository.save(SalaNueva);
	}

	/**
	 * Metodo encargado de eliminar una sala por id
	 * 
	 * @author aperez
	 * 
	 * @param salaDTO
	 * @return Sala
	 */
	@DeleteMapping("/eliminarSala/{idsala}")
	public Map<String, String> eliminarSala(@PathVariable(value = "idsala") Long idsala) {
		logger.debug("Inicio eliminarSucursal");
		Map<String, String> respuesta = new HashMap<>();
		try {
			//Se busca la sala a eliminar
			Sala sala = salaRepository.findById(idsala).get();
			salaRepository.delete(sala);
			respuesta.put("se ha eliminado la sala", "OK");
		} catch (Exception e) {
			respuesta.put("Error al eliminar la sala", e.getMessage());

		}

		return respuesta;
	}

}
