package com.co.ingeneo.cinema.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.co.ingeneo.cinema.dto.SucursalDTO;
import com.co.ingeneo.cinema.model.Sucursal;
import com.co.ingeneo.cinema.repository.SucursalRepository;
import com.co.ingeneo.cinema.utils.ConstruirDTOsUtils;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE,
		RequestMethod.PUT })
@RequestMapping("/controlSucursalRest")
public class SucursalControlador {

	/**
	 * Se crea instancia del logger para imprimir en consola
	 * 
	 */
	private static Logger logger = LoggerFactory.getLogger(SucursalControlador.class.getName());

	/**
	 * Se crea instancia de la interfaz de la sucursal encargada de realizar todas
	 * las acciones de base de datos a través de JPA
	 * 
	 */
	@Autowired
	private SucursalRepository sucursalRepository;

	ConstruirDTOsUtils construirDTOsUtils = new ConstruirDTOsUtils();

	/**
	 * Metodo encargado de consultar una lista de sucursales
	 * 
	 * @author aperez
	 * 
	 * @param sucursalDTO
	 * @return Sucursal
	 */
	@GetMapping("/consultarSucursales")
	public List<SucursalDTO> consultarSucursales() {
		logger.info("Inicio consultarSucursales");
		return construirDTOsUtils.castearListaSucursalesEntidadADTO(sucursalRepository.findAll());

	}

	/**
	 * Metodo encargado de consultar una sucursal por id
	 * 
	 * @author aperez
	 * 
	 * @param sucursalDTO
	 * @return Sucursal
	 */
	@GetMapping("/consultarSucursalPorId/{idsucursal}")
	public ResponseEntity<Sucursal> consultarSucursalPorId(@PathVariable(value = "idsucursal") Long idsucursal) {
		logger.info("Inicio consultarSucursalPorId");
		Sucursal sucursal = sucursalRepository.findById(idsucursal).get();
		return ResponseEntity.ok().body(sucursal);
	}

	/**
	 * Metodo encargado de crear una sucursal
	 * 
	 * @author aperez
	 * 
	 * @param sucursalDTO
	 * @return Sucursal
	 */
	@PostMapping("/crearSucursal")
	public Sucursal crearSucursal(@Valid @RequestBody SucursalDTO sucursalDTO) {
		logger.debug("Inicio crearSucursal");
		
		Sucursal sucursalNueva = construirDTOsUtils.castearSucursalDTOAEntidad(sucursalDTO);
		return sucursalRepository.save(sucursalNueva);
	}

	/**
	 * Metodo encargado de eliminar una sucursal por id
	 * 
	 * @author aperez
	 * 
	 * @param sucursalDTO
	 * @return Sucursal
	 */
	@DeleteMapping("/eliminarSucursal/{idsucursal}")
	public Map<String, String> eliminarSucursal(@PathVariable(value = "idsucursal") Long idsucursal) {
		logger.debug("Inicio eliminarSucursal");
		Map<String, String> respuesta = new HashMap<>();
		try {
			//Se busca la sucursal a eliminar
			Sucursal sucursal = sucursalRepository.findById(idsucursal).get();
			sucursalRepository.delete(sucursal);
			respuesta.put("se ha eliminado la sucursal", "OK");
		} catch (Exception e) {
			respuesta.put("Error al eliminar la sucursal", e.getMessage());

		}

		return respuesta;
	}

}
