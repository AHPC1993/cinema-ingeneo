/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.co.ingeneo.cinema.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlTransient;

import com.co.ingeneo.cinema.model.Administrador;
import com.co.ingeneo.cinema.model.Agenda;
import com.co.ingeneo.cinema.model.PaTipoSala;
import com.co.ingeneo.cinema.model.SalaFilas;
import com.co.ingeneo.cinema.model.Sucursal;

/**
 *
 * @author allan
 */
public class SalaDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long idSala;
    private String nombre;
    private String observaciones;
    private Date fechaCreacion;
    private String usuarioCreacion;
    private Long idSucursal;
    private Long idTipoSala;
    private Long idAdministrador;

    public SalaDTO() {
    }

    public SalaDTO(Long idSala) {
        this.idSala = idSala;
    }

	/**
	 * @return the idSala
	 */
	public Long getIdSala() {
		return idSala;
	}

	/**
	 * @param idSala the idSala to set
	 */
	public void setIdSala(Long idSala) {
		this.idSala = idSala;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the observaciones
	 */
	public String getObservaciones() {
		return observaciones;
	}

	/**
	 * @param observaciones the observaciones to set
	 */
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	/**
	 * @return the fechaCreacion
	 */
	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	/**
	 * @return the usuarioCreacion
	 */
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	/**
	 * @param usuarioCreacion the usuarioCreacion to set
	 */
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	/**
	 * @return the idSucursal
	 */
	public Long getIdSucursal() {
		return idSucursal;
	}

	/**
	 * @param idSucursal the idSucursal to set
	 */
	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}

	/**
	 * @return the idTipoSala
	 */
	public Long getIdTipoSala() {
		return idTipoSala;
	}

	/**
	 * @param idTipoSala the idTipoSala to set
	 */
	public void setIdTipoSala(Long idTipoSala) {
		this.idTipoSala = idTipoSala;
	}

	/**
	 * @return the idAdministrador
	 */
	public Long getIdAdministrador() {
		return idAdministrador;
	}

	/**
	 * @param idAdministrador the idAdministrador to set
	 */
	public void setIdAdministrador(Long idAdministrador) {
		this.idAdministrador = idAdministrador;
	}


    
    
}
