/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.co.ingeneo.cinema.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "PA_DEPARTAMENTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaDepartamento.findAll", query = "SELECT p FROM PaDepartamento p")
    , @NamedQuery(name = "PaDepartamento.findByIdDepartamento", query = "SELECT p FROM PaDepartamento p WHERE p.idDepartamento = :idDepartamento")
    , @NamedQuery(name = "PaDepartamento.findByNombre", query = "SELECT p FROM PaDepartamento p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "PaDepartamento.findByObservaciones", query = "SELECT p FROM PaDepartamento p WHERE p.observaciones = :observaciones")})
public class PaDepartamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_DEPARTAMENTO")
    private Long idDepartamento;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDepartamento")
    private List<Ciudad> ciudadList;

    public PaDepartamento() {
    }

    public PaDepartamento(Long idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public PaDepartamento(Long idDepartamento, String nombre) {
        this.idDepartamento = idDepartamento;
        this.nombre = nombre;
    }

    public Long getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Long idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @XmlTransient
    public List<Ciudad> getCiudadList() {
        return ciudadList;
    }

    public void setCiudadList(List<Ciudad> ciudadList) {
        this.ciudadList = ciudadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDepartamento != null ? idDepartamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaDepartamento)) {
            return false;
        }
        PaDepartamento other = (PaDepartamento) object;
        if ((this.idDepartamento == null && other.idDepartamento != null) || (this.idDepartamento != null && !this.idDepartamento.equals(other.idDepartamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.co.ingeneo.cinema.model.PaDepartamento[ idDepartamento=" + idDepartamento + " ]";
    }
    
}
