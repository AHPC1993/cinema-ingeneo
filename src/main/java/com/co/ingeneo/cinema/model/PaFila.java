/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.co.ingeneo.cinema.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "PA_FILA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaFila.findAll", query = "SELECT p FROM PaFila p")
    , @NamedQuery(name = "PaFila.findByIdFila", query = "SELECT p FROM PaFila p WHERE p.idFila = :idFila")
    , @NamedQuery(name = "PaFila.findByNombre", query = "SELECT p FROM PaFila p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "PaFila.findByNumeroSillas", query = "SELECT p FROM PaFila p WHERE p.numeroSillas = :numeroSillas")
    , @NamedQuery(name = "PaFila.findByObservaciones", query = "SELECT p FROM PaFila p WHERE p.observaciones = :observaciones")})
public class PaFila implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_FILA")
    private Long idFila;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "NUMERO_SILLAS")
    private int numeroSillas;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFila")
    private List<SalaFilas> salaFilasList;

    public PaFila() {
    }

    public PaFila(Long idFila) {
        this.idFila = idFila;
    }

    public PaFila(Long idFila, String nombre, int numeroSillas) {
        this.idFila = idFila;
        this.nombre = nombre;
        this.numeroSillas = numeroSillas;
    }

    public Long getIdFila() {
        return idFila;
    }

    public void setIdFila(Long idFila) {
        this.idFila = idFila;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumeroSillas() {
        return numeroSillas;
    }

    public void setNumeroSillas(int numeroSillas) {
        this.numeroSillas = numeroSillas;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @XmlTransient
    public List<SalaFilas> getSalaFilasList() {
        return salaFilasList;
    }

    public void setSalaFilasList(List<SalaFilas> salaFilasList) {
        this.salaFilasList = salaFilasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFila != null ? idFila.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaFila)) {
            return false;
        }
        PaFila other = (PaFila) object;
        if ((this.idFila == null && other.idFila != null) || (this.idFila != null && !this.idFila.equals(other.idFila))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.co.ingeneo.cinema.model.PaFila[ idFila=" + idFila + " ]";
    }
    
}
