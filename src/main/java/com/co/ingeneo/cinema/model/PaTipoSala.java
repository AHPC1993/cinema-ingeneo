/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.co.ingeneo.cinema.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "PA_TIPO_SALA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaTipoSala.findAll", query = "SELECT p FROM PaTipoSala p")
    , @NamedQuery(name = "PaTipoSala.findByIdTipoSala", query = "SELECT p FROM PaTipoSala p WHERE p.idTipoSala = :idTipoSala")
    , @NamedQuery(name = "PaTipoSala.findByValor", query = "SELECT p FROM PaTipoSala p WHERE p.valor = :valor")
    , @NamedQuery(name = "PaTipoSala.findByObservaciones", query = "SELECT p FROM PaTipoSala p WHERE p.observaciones = :observaciones")})
public class PaTipoSala implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID_TIPO_SALA")
    private Long idTipoSala;
    @Basic(optional = false)
    @Column(name = "VALOR")
    private String valor;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoSala")
    private List<Sala> salaList;

    public PaTipoSala() {
    }

    public PaTipoSala(Long idTipoSala) {
        this.idTipoSala = idTipoSala;
    }

    public PaTipoSala(Long idTipoSala, String valor) {
        this.idTipoSala = idTipoSala;
        this.valor = valor;
    }

    public Long getIdTipoSala() {
        return idTipoSala;
    }

    public void setIdTipoSala(Long idTipoSala) {
        this.idTipoSala = idTipoSala;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @XmlTransient
    public List<Sala> getSalaList() {
        return salaList;
    }

    public void setSalaList(List<Sala> salaList) {
        this.salaList = salaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoSala != null ? idTipoSala.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaTipoSala)) {
            return false;
        }
        PaTipoSala other = (PaTipoSala) object;
        if ((this.idTipoSala == null && other.idTipoSala != null) || (this.idTipoSala != null && !this.idTipoSala.equals(other.idTipoSala))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.co.ingeneo.cinema.model.PaTipoSala[ idTipoSala=" + idTipoSala + " ]";
    }
    
}
