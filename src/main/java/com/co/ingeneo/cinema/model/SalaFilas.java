/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.co.ingeneo.cinema.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author allan
 */
@Entity
@Table(name = "SALA_FILAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SalaFilas.findAll", query = "SELECT s FROM SalaFilas s")
    , @NamedQuery(name = "SalaFilas.findByIdSalaFila", query = "SELECT s FROM SalaFilas s WHERE s.idSalaFila = :idSalaFila")
    , @NamedQuery(name = "SalaFilas.findByNumeroFilas", query = "SELECT s FROM SalaFilas s WHERE s.numeroFilas = :numeroFilas")
    , @NamedQuery(name = "SalaFilas.findByObservaciones", query = "SELECT s FROM SalaFilas s WHERE s.observaciones = :observaciones")
    , @NamedQuery(name = "SalaFilas.findByFechaCreacion", query = "SELECT s FROM SalaFilas s WHERE s.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "SalaFilas.findByUsuarioCreacion", query = "SELECT s FROM SalaFilas s WHERE s.usuarioCreacion = :usuarioCreacion")})
public class SalaFilas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_SALA_FILA")
    private Long idSalaFila;
    @Basic(optional = false)
    @Column(name = "NUMERO_FILAS")
    private int numeroFilas;
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Column(name = "USUARIO_CREACION")
    private String usuarioCreacion;
    @JoinColumn(name = "ID_SALA", referencedColumnName = "ID_SALA")
    @ManyToOne(optional = false)
    private Sala idSala;
    @JoinColumn(name = "ID_FILA", referencedColumnName = "ID_FILA")
    @ManyToOne(optional = false)
    private PaFila idFila;

    public SalaFilas() {
    }

    public SalaFilas(Long idSalaFila) {
        this.idSalaFila = idSalaFila;
    }

    public SalaFilas(Long idSalaFila, int numeroFilas) {
        this.idSalaFila = idSalaFila;
        this.numeroFilas = numeroFilas;
    }

    public Long getIdSalaFila() {
        return idSalaFila;
    }

    public void setIdSalaFila(Long idSalaFila) {
        this.idSalaFila = idSalaFila;
    }

    public int getNumeroFilas() {
        return numeroFilas;
    }

    public void setNumeroFilas(int numeroFilas) {
        this.numeroFilas = numeroFilas;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Sala getIdSala() {
        return idSala;
    }

    public void setIdSala(Sala idSala) {
        this.idSala = idSala;
    }

    public PaFila getIdFila() {
        return idFila;
    }

    public void setIdFila(PaFila idFila) {
        this.idFila = idFila;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSalaFila != null ? idSalaFila.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalaFilas)) {
            return false;
        }
        SalaFilas other = (SalaFilas) object;
        if ((this.idSalaFila == null && other.idSalaFila != null) || (this.idSalaFila != null && !this.idSalaFila.equals(other.idSalaFila))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.co.ingeneo.cinema.model.SalaFilas[ idSalaFila=" + idSalaFila + " ]";
    }
    
}
