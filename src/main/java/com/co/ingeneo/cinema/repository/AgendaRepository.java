package com.co.ingeneo.cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.co.ingeneo.cinema.model.Agenda;

/**
 * Interfaz encargada de crear una comunicación hacia la base de datos por medio
 * de JPA para realizar diferentes acciones para la agenda
 * 
 * @author aperez
 */
@Repository
public interface AgendaRepository extends JpaRepository<Agenda, Long> {

}
