package com.co.ingeneo.cinema.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.co.ingeneo.cinema.model.Reserva;

/**
 * Interfaz encargada de crear una comunicación hacia la base de datos por medio
 * de JPA para realizar diferentes acciones para las reservas
 * 
 * @author aperez
 */
@Repository
public interface ReservaRepository extends JpaRepository<Reserva, Long> {

}
