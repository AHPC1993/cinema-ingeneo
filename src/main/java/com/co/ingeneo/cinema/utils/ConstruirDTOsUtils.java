package com.co.ingeneo.cinema.utils;

import java.util.ArrayList;
import java.util.List;

import com.co.ingeneo.cinema.dto.SalaDTO;
import com.co.ingeneo.cinema.dto.SucursalDTO;
import com.co.ingeneo.cinema.model.Administrador;
import com.co.ingeneo.cinema.model.Ciudad;
import com.co.ingeneo.cinema.model.PaTipoSala;
import com.co.ingeneo.cinema.model.Sala;
import com.co.ingeneo.cinema.model.Sucursal;

/**
 * <b>Descripción:<b> Clase que se encarga de hacer un casteo de un DTO a una
 * entidad y de misma forma una entidad en un DTO
 * 
 * @author aperez
 * @version
 */
public class ConstruirDTOsUtils {

	/**
	 * Metodo encargado de convertir una lista de una entidad sucursal a una lista
	 * DTO de sucursales
	 * 
	 * 
	 * @author aperez
	 * 
	 * @param List<Sucursal>
	 * @return List<SucursalDTO>
	 */
	public List<SucursalDTO> castearListaSucursalesEntidadADTO(List<Sucursal> listaSucursales) {
		List<SucursalDTO> listaSucursalesDTO = new ArrayList<SucursalDTO>();
		for (Sucursal sucursal : listaSucursales) {
			listaSucursalesDTO.add(castearSucursalEntidadADTO(sucursal));
		}

		return listaSucursalesDTO;
	}

	/**
	 * Metodo encargado de convertir una entidad Sucursal a un DTO SucursalDTO
	 * 
	 * 
	 * @author aperez
	 * 
	 * @param sucursal
	 * @return SucursalDTO
	 */
	public SucursalDTO castearSucursalEntidadADTO(Sucursal sucursal) {
		SucursalDTO sucursalDTO = null;
		if (sucursal != null) {
			sucursalDTO = new SucursalDTO();
			sucursalDTO.setDireccion(sucursal.getDireccion());
			sucursalDTO.setFechaCreacion(sucursal.getFechaCreacion());
			sucursalDTO.setIdAdministrador(sucursal.getIdAdministrador().getIdAdministrador());
			sucursalDTO.setIdCiudad(sucursal.getIdCiudad().getIdCiudad());
			sucursalDTO.setIdSucursal(sucursal.getIdSucursal());
			sucursalDTO.setNombre(sucursal.getNombre());
			sucursalDTO.setObservaciones(sucursal.getObservaciones());
			sucursalDTO.setSalaList(sucursal.getSalaList());
		}

		return sucursalDTO;
	}

	/**
	 * Metodo encargado de convertir un DTO de Sucursal a una entidad Sucursal
	 * 
	 * 
	 * @author aperez
	 * 
	 * @param SucursalDTO
	 * @return Sucursal
	 */
	public Sucursal castearSucursalDTOAEntidad(SucursalDTO sucursalDTO) {
		Sucursal sucursal = null;
		if (sucursalDTO != null) {
			sucursal = new Sucursal();
			sucursal.setDireccion(sucursalDTO.getDireccion());
			sucursal.setFechaCreacion(sucursalDTO.getFechaCreacion());
			sucursal.setIdAdministrador(new Administrador(sucursalDTO.getIdAdministrador()));
			sucursal.setIdCiudad(new Ciudad(sucursalDTO.getIdCiudad()));
			sucursal.setIdSucursal(sucursalDTO.getIdSucursal());
			sucursal.setNombre(sucursalDTO.getNombre());
			sucursal.setObservaciones(sucursalDTO.getObservaciones());
			sucursal.setSalaList(sucursalDTO.getSalaList());
		}

		return sucursal;
	}
	
	
	/**
	 * Metodo encargado de convertir una lista de una entidad Sala a una lista
	 * DTO de salas
	 * 
	 * 
	 * @author aperez
	 * 
	 * @param List<Sala>
	 * @return List<SalaDTO>
	 */
	public List<SalaDTO> castearListaSalasEntidadADTO(List<Sala> listaSalas) {
		List<SalaDTO> listaSalasDTO = new ArrayList<SalaDTO>();
		for (Sala sala : listaSalas) {
			listaSalasDTO.add(castearSalaEntidadADTO(sala));
		}

		return listaSalasDTO;
	}

	/**
	 * Metodo encargado de convertir una entidad Sala a un DTO SalalDTO
	 * 
	 * 
	 * @author aperez
	 * 
	 * @param sala
	 * @return SalalDTO
	 */
	public SalaDTO castearSalaEntidadADTO(Sala sala) {
		SalaDTO salaDTO = null;
		if (sala != null) {
			salaDTO = new SalaDTO();
			salaDTO.setFechaCreacion(sala.getFechaCreacion());
			salaDTO.setIdAdministrador(sala.getIdAdministrador().getIdAdministrador());
			salaDTO.setIdSala(sala.getIdSala());
			salaDTO.setIdSucursal(sala.getIdSucursal().getIdSucursal());
			salaDTO.setIdTipoSala(sala.getIdTipoSala().getIdTipoSala());
			salaDTO.setNombre(sala.getNombre());
			salaDTO.setObservaciones(sala.getObservaciones());


		}
		return salaDTO;
	}

	/**
	 * Metodo encargado de convertir un DTO de Sucursal a una entidad Sucursal
	 * 
	 * 
	 * @author aperez
	 * 
	 * @param salaDTO
	 * @return Sala
	 */
	public Sala castearSalaDTOAEntidad(SalaDTO salaDTO) {
		Sala sala = null;
		if (salaDTO != null) {
			sala = new Sala();
			sala.setFechaCreacion(salaDTO.getFechaCreacion());
			sala.setIdAdministrador(new Administrador(salaDTO.getIdAdministrador()));
			sala.setIdSala(sala.getIdSala());
			sala.setIdSucursal(new Sucursal(salaDTO.getIdSucursal()));
			sala.setIdTipoSala(new PaTipoSala(salaDTO.getIdTipoSala()));
			sala.setNombre(salaDTO.getNombre());
			salaDTO.setObservaciones(salaDTO.getObservaciones());

		}
		return sala;
	}

}
